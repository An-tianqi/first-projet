import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.express as px
import psutil
import mysql.connector
import time
import pandas as pd
import datetime

# 连接到 MySQL 数据库
try:
    cnx = mysql.connector.connect(
        host="localhost",
        port="3306",
        user="root",
        password="atq970905",
        database="cpu"
    )
except mysql.connector.Error as err:
    print("Error:", err)

def insert_cpu_data(cpu_percent):
    try:
        # 插入 CPU 数据到数据库
        cursor = cnx.cursor()
        cursor.execute('''INSERT INTO cpu_data (cpu_percent)
                          VALUES (%s)''', (cpu_percent,))
        cnx.commit()
        print("CPU data inserted successfully.")
    except Exception as e:
        print("Error inserting CPU data:", e)

def fetch_cpu_data():
    try:
        # 从数据库中获取 CPU 数据
        cursor = cnx.cursor()
        cursor.execute("SELECT * FROM cpu_data")
        rows = cursor.fetchall()
        df = pd.DataFrame(rows, columns=['id', 'cpu_percent'])
        return df
    except Exception as e:
        print("Error fetching CPU data:", e)

if __name__ == "__main__":
    cpu_percent = 70.0 
    insert_cpu_data(cpu_percent)

    cpu_data = fetch_cpu_data()
    print("CPU Data:" cpu_data)

