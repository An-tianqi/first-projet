import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.express as px
import psutil
import mysql.connector
import time
import pandas as pd
import datetime

# 连接到 MySQL 数据库
cnx = mysql.connector.connect(
    host="localhost",
    user="root",
    password="atq970905",
    database="cpu"
)

# 创建 Dash 应用
app = dash.Dash(__name__)

# 创建仪表板布局
app.layout = html.Div([
    html.H1("System Information Dashboard"),
    html.Div(id='boot-time'),
    dcc.Interval(
        id='boot-time-interval',
        interval=60000,  # 每 60 秒更新一次
        n_intervals=0
    ),
    dcc.Graph(id='cpu-graph'),
    dcc.Interval(
        id='cpu-graph-interval',
        interval=5000,  # 每 5 秒更新一次
        n_intervals=0
    )
])

@app.callback(
    Output('boot-time', 'children'),
    [Input('interval-component', 'n_intervals')]
)
def update_boot_time(n):
    # 获取系统启动时间
    boot_time_seconds = psutil.boot_time()
    boot_time = datetime.datetime.fromtimestamp(boot_time_seconds)
    return f"System Boot Time: {boot_time}"

# 回调函数：更新图表数据
@app.callback(
    Output('cpu-graph', 'figure'),
    [Input('interval-component', 'n_intervals')]
)
def update_graph(n):
    # 从数据库中获取 CPU 数据
    cursor = cnx.cursor()
    cursor.execute("SELECT * FROM cpu_data")
    rows = cursor.fetchall()
    df = pd.DataFrame(rows, columns=['id', 'cpu_percent'])
    fig = px.line(df, x='id', y='cpu_percent', title='CPU Usage')
    return fig

def get_cpu_data():
    cpu_percent = psutil.cpu_percent()
    return cpu_percent

def save_to_database(cpu_data):
    cursor = cnx.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS cpu_data
                      (id INT AUTO_INCREMENT PRIMARY KEY, cpu_percent FLOAT)''')

    cursor.execute('''INSERT INTO cpu_data (cpu_percent)
                      VALUES (%s)''', (cpu_data,))
    
    cnx.commit()

if __name__ == "__main__":
    # 创建 CPU 数据表
    cursor = cnx.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS cpu_data
                      (id INT AUTO_INCREMENT PRIMARY KEY, cpu_percent FLOAT)''')
    cnx.commit()

    # 启动 Dash 应用
    app.run_server(debug=True, use_reloader=False)
    
    # 每 5 秒获取一次 CPU 数据并存储到数据库中
    while True:
        cpu_data = get_cpu_data()
        save_to_database(cpu_data)
        time.sleep(5)
