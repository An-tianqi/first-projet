dash==2.0.0
plotly==5.3.1
psutil==5.8.0
mysql-connector-python==8.0.27
pandas==1.3.3
datetime==4.3
werkzeug==2.0.2